import { callApi } from '../helpers/apiHelper';
import { FighterObject, FighterDetailsObject } from '../helpers/mockData';

class FighterService {
  async getFighters(): Promise<FighterObject[]> {
    try {
      const endpoint: string = 'fighters.json';
      const apiResult = <FighterObject[]>await callApi(endpoint, 'GET');

      return <FighterObject[]>apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id: string): Promise<FighterDetailsObject> {
    try {
      const endpoint: string = `details/fighter/${id}.json`;
      const apiResult = <FighterDetailsObject>await callApi(endpoint, 'GET');
      if(!apiResult) {
        throw Error('Such fighter not found');
      }

      return <FighterDetailsObject>apiResult;
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
