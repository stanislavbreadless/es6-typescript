import { createElement } from '../helpers/domHelper';
import { renderArena } from './arena';
import * as versusImg from '../../../resources/versus.png';
import { createFighterPreview } from './fighterPreview';
import { fighterService } from '../services/fightersService';
import { FighterDetailsObject } from '../helpers/mockData';

export type FighterSelector = (fighterId: string) => Promise<void>;
export function createFightersSelector(): FighterSelector {
  let selectedFighters: FighterDetailsObject[] = [];

  return async (fighterId: string) => {
    const fighter = <FighterDetailsObject> await getFighterInfo(fighterId);
    const [playerOne, playerTwo] = selectedFighters;
    const firstFighter: FighterDetailsObject|undefined = playerOne ?? fighter;
    const secondFighter: FighterDetailsObject|undefined = Boolean(playerOne) ? playerTwo ?? fighter : playerTwo;
    selectedFighters = [firstFighter, secondFighter];

    renderSelectedFighters(selectedFighters);
  };
}

const fighterDetailsMap = new Map<string, FighterDetailsObject>();

export async function getFighterInfo(fighterId: string): Promise<FighterDetailsObject> {
  if (fighterDetailsMap.has(fighterId)) {
    return <FighterDetailsObject>fighterDetailsMap.get(fighterId);
  }

  const fighterDetails: FighterDetailsObject = await fighterService.getFighterDetails(fighterId);
  fighterDetailsMap.set(fighterId, fighterDetails);
  return fighterDetails;
}

function renderSelectedFighters(selectedFighters: FighterDetailsObject[]): void {
  const fightersPreview = <HTMLElement>document.querySelector('.preview-container___root');
  const [playerOne, playerTwo] = selectedFighters;
  const firstPreview = <HTMLElement>createFighterPreview(playerOne, 'left');
  const secondPreview = <HTMLElement>createFighterPreview(playerTwo, 'right');
  const versusBlock = <HTMLElement>createVersusBlock(selectedFighters);

  fightersPreview.innerHTML = '';
  fightersPreview.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters: FighterDetailsObject[]): HTMLElement {
  const canStartFight: boolean = selectedFighters.filter(Boolean).length === 2;
  const onClick = () => startFight(<[FighterDetailsObject, FighterDetailsObject]>selectedFighters);
  const container: HTMLElement = createElement({ tagName: 'div', className: 'preview-container___versus-block' });
  const image: HTMLElement = createElement({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: { src: versusImg },
  });
  const disabledBtn: string = canStartFight ? '' : 'disabled';
  const fightBtn: HTMLElement = createElement({
    tagName: 'button',
    className: `preview-container___fight-btn ${disabledBtn}`,
  });

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters: [FighterDetailsObject, FighterDetailsObject]): void {
  renderArena(selectedFighters);
}
