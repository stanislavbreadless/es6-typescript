import { createElement, ElementAttributesObject } from '../helpers/domHelper';
import { createFightersSelector, FighterSelector } from './fighterSelector';
import { FighterObject } from '../helpers/mockData';

export function createFighters(fighters: FighterObject[]): HTMLElement {
  const selectFighter: FighterSelector  = createFightersSelector();
  const container: HTMLElement = createElement({ tagName: 'div', className: 'fighters___root' });
  const preview: HTMLElement = createElement({ tagName: 'div', className: 'preview-container___root' });
  const fightersList: HTMLElement = createElement({ tagName: 'div', className: 'fighters___list' });
  const fighterElements: HTMLElement[] = fighters.map((fighter) => createFighter(fighter, selectFighter));

  fightersList.append(...fighterElements);
  container.append(preview, fightersList);

  return container;
}

function createFighter(fighter: FighterObject, selectFighter: FighterSelector):HTMLElement {
  const fighterElement:HTMLElement = createElement({ tagName: 'div', className: 'fighters___fighter' });
  const imageElement: HTMLElement = createImage(fighter);
  const onClick = () => selectFighter(fighter._id);

  fighterElement.append(imageElement);
  fighterElement.addEventListener('click', onClick, false);

  return fighterElement;
}

function createImage(fighter: FighterObject): HTMLElement {
  const { source, name } = fighter;
  const attributes: ElementAttributesObject = { 
    src: source,
    title: name,
    alt: name, 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter___fighter-image',
    attributes
  });

  return imgElement;
}