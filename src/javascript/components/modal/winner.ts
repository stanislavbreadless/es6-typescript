import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import { FighterDetailsObject } from '../../helpers/mockData';

export function showWinnerModal(fighter: FighterDetailsObject): void {
  const winnerDescription: HTMLElement = createElement({
    tagName: 'div',
    className: 'modal-body',
  });

  const winnerPicture: HTMLElement = createElement({
    tagName: 'img',
    className: 'modal-img',
    attributes: {
      src: fighter.source
    }
  })

  winnerDescription.append(winnerPicture);

  showModal({
    title: `${fighter.name} Wins!`,
    bodyElement: winnerDescription
  })
}
