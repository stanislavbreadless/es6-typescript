import { ElementAttributesObject, createElement } from '../helpers/domHelper';
import { FighterDetailsObject } from '../helpers/mockData';

export function createFighterPreview(fighter: FighterDetailsObject, position: 'left'|'right'): HTMLElement|null {
  const positionClassName:string = position === 'right' 
                                   ? 'fighter-preview___right' 
                                   : 'fighter-preview___left';
  const fighterElement: HTMLElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (!fighter) {
    return null;
  }

  const fighterImage: HTMLElement = createFighterImage(fighter);
  const fighterName: HTMLElement = createFighterTextDiv(fighter, 'Name');
  const fighterHelth: HTMLElement = createFighterTextDiv(fighter, 'Health');
  const fighterAttack: HTMLElement = createFighterTextDiv(fighter, 'Attack');
  const fighterDefense: HTMLElement = createFighterTextDiv(fighter, 'Defense');

  fighterElement.append(fighterImage, fighterName, fighterHelth, fighterAttack, fighterDefense);
  return fighterElement;
}

export function createFighterImage(fighter: FighterDetailsObject): HTMLElement {
  const { source, name } = fighter;
  const attributes: ElementAttributesObject = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement: HTMLElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

export function createFighterTextDiv(fighter: FighterDetailsObject, attr: string): HTMLElement {
  const textElement: HTMLElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___text',
  });
  textElement.innerText = `${attr}: ${fighter[attr.toLowerCase()]}`;
  return textElement;
}
