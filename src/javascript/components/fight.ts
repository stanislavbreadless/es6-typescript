import { controls } from '../../constants/controls';
import { FighterDetailsObject } from '../helpers/mockData';

enum FighterOrder { First, Second };

class ArenaFigher{
  constructor(private fighter: FighterDetailsObject, public order: FighterOrder) {
    this.health = fighter.health;
    this.maxHealth = fighter.health;
    this.attack = fighter.attack;
    this.defense = fighter.defense;
    this.isBlocking = false;
    this.isComboAvaliable = true; 
  }
  
  health: number
  readonly maxHealth: number
  attack: number
  defense: number
  isBlocking: boolean
  isComboAvaliable: boolean 

  getRealFighterObj() {
    return this.fighter;
  }
}

interface GameState {
  readonly firstFighter: ArenaFigher,
  readonly secondFighter: ArenaFigher,
  pressedKeys: Set<string>
}

function reduceHealth(fighter: ArenaFigher, amount: number): void {
  fighter.health -= amount;
  if (fighter.health < 0)
    fighter.health = 0;

  if (fighter.order === FighterOrder.First) {
    const leftHealthBar = <HTMLElement>document.getElementById('left-fighter-indicator');
    leftHealthBar.style.width = (fighter.health / fighter.maxHealth) * 100 + '%';
  }
  else {
    const rightHealthBar = <HTMLElement>document.getElementById('right-fighter-indicator');
    rightHealthBar.style.width = (fighter.health / fighter.maxHealth) * 100 + '%';
  }
}

function checkHealth(firstFighter: ArenaFigher, 
                     secondFighter: ArenaFigher, 
                     endGame: (winner: ArenaFigher) => void
): void {
  if (firstFighter.health <= 0)
    endGame(secondFighter);
  if (secondFighter.health <= 0)
    endGame(firstFighter);
}

function checkForCombo(pressedKeys: Set<string>, comboKeys: string[]): boolean {
  return comboKeys.every(x => pressedKeys.has(x));
}

function checkAttackAndBlockEvents(gameState : GameState, code: string): void {
  const {
    firstFighter,
    secondFighter,
  } = gameState;

  switch (code) {
    case controls.PlayerOneAttack:
      if (!firstFighter.isBlocking && !secondFighter.isBlocking)
        reduceHealth(secondFighter, getDamage(firstFighter, secondFighter));
      break;
    case controls.PlayerOneBlock:
      firstFighter.isBlocking = true;
      break;
    case controls.PlayerTwoAttack:
      if (!firstFighter.isBlocking && !secondFighter.isBlocking)
        reduceHealth(firstFighter, getDamage(secondFighter, firstFighter));
      break;
    case controls.PlayerTwoBlock:
      secondFighter.isBlocking = true;
      break;
  }
}

function checkComboEvents(gameState: GameState): void {
  const {
    firstFighter,
    secondFighter,
    pressedKeys
  } = gameState;

  if (firstFighter.isComboAvaliable && !firstFighter.isBlocking
    && checkForCombo(pressedKeys, controls.PlayerOneCriticalHitCombination)) {

    reduceHealth(secondFighter, firstFighter.attack * 2)

    firstFighter.isComboAvaliable = false;
    setTimeout(() => firstFighter.isComboAvaliable = true, 10000);
  }

  if (secondFighter.isComboAvaliable && !secondFighter.isBlocking
    && checkForCombo(pressedKeys, controls.PlayerTwoCriticalHitCombination)) {

    reduceHealth(firstFighter, secondFighter.attack * 2);

    secondFighter.isComboAvaliable = false;
    setTimeout(() => secondFighter.isComboAvaliable = true, 10000);
  }
}

interface KeyboardController {
  keyDownController(e: KeyboardEvent):void,
  keyUpController(e: KeyboardEvent):void
}

function getKeyboardControls(firstFighter: ArenaFigher, 
                             secondFighter: ArenaFigher, 
                             endGame: (winner: ArenaFigher) => void
) : KeyboardController {
  const gameState = <GameState>{
    pressedKeys: new Set(),
    firstFighter,
    secondFighter
  };

  return {
    keyDownController(e: KeyboardEvent) {
      gameState.pressedKeys.add(e.code);

      checkAttackAndBlockEvents(gameState, e.code);
      checkComboEvents(gameState);

      checkHealth(firstFighter, secondFighter, endGame);
    },

    keyUpController(e: KeyboardEvent) {
      switch (e.code) {
        case controls.PlayerOneBlock:
          firstFighter.isBlocking = false;
          break;
        case controls.PlayerTwoBlock:
          secondFighter.isBlocking = false;
          break;
      }
      gameState.pressedKeys.delete(e.code);
    }
  }
}

function getArenaFighter(fighter: FighterDetailsObject, order: FighterOrder): ArenaFigher {
  return new ArenaFigher(fighter, order);
}

export async function fight(firstFighter: FighterDetailsObject, 
                            secondFighter: FighterDetailsObject
): Promise<FighterDetailsObject> {
  return new Promise((resolve) => {
    const arenaFirstFighter = getArenaFighter(firstFighter, FighterOrder.First);
    const arenaSecondFighter = getArenaFighter(secondFighter, FighterOrder.Second);

    const endGame = (winner: ArenaFigher) => {
      window.removeEventListener('keyup', keyUpController);
      window.removeEventListener('keydown', keyDownController);
      resolve(winner.getRealFighterObj());
    }
    const { keyUpController, keyDownController } = getKeyboardControls(arenaFirstFighter, arenaSecondFighter, endGame);

    window.addEventListener('keyup', keyUpController);
    window.addEventListener('keydown', keyDownController);
  });
}

export function getDamage(attacker: ArenaFigher, defender: ArenaFigher): number {
  const damage = getHitPower(attacker) - getBlockPower(defender);

  return damage >= 0 ? damage : 0;
}

function randomInRange(l: number, r: number): number {
  return (Math.random() * (r-l)) + l;
}

export function getHitPower(fighter: ArenaFigher): number {
  return fighter.attack * randomInRange(1,2);
}

export function getBlockPower(fighter: ArenaFigher): number {
  return fighter.defense * randomInRange(1,2);
}
