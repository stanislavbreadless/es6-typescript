import { createFighters } from './components/fightersView';
import { fighterService } from './services/fightersService';
import { FighterObject } from './helpers/mockData';

class App {
  constructor() {
    this.startApp();
  }

  static readonly rootElement = <HTMLElement>document.getElementById('root');
  static readonly loadingElement = <HTMLElement>document.getElementById('loading-overlay');

  async startApp() {
    try {
      App.loadingElement.style.visibility = 'visible';

      const fighters: FighterObject[] = await fighterService.getFighters();
      const fightersElement: HTMLElement = createFighters(fighters as FighterObject[]);

      App.rootElement.appendChild(fightersElement);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }
}

export default App;
