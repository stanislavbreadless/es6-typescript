export interface ElementAttributesObject {
  [key: string] : string
}

export interface ElementDescriptor {
  readonly tagName: string,
  readonly className?: string,
  readonly attributes?: ElementAttributesObject
}

export function createElement(elementObject: ElementDescriptor): HTMLElement {
  const { tagName, className='', attributes = {} } = elementObject;

  const element: HTMLElement = document.createElement(tagName);

  if (className) {
    const classNames: string[] = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  Object.keys(attributes).forEach((key) => element.setAttribute(key, attributes[key]));

  return element;
}
