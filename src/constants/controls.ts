interface ControlsObject {
  readonly PlayerOneAttack: string,
  readonly PlayerOneBlock: string,
  readonly PlayerTwoAttack: string,
  readonly PlayerTwoBlock: string,
  readonly PlayerOneCriticalHitCombination: string[],
  readonly PlayerTwoCriticalHitCombination: string[]
}

export const controls: ControlsObject = {
  PlayerOneAttack: 'KeyA',
  PlayerOneBlock: 'KeyD',
  PlayerTwoAttack: 'KeyJ',
  PlayerTwoBlock: 'KeyL',
  PlayerOneCriticalHitCombination: ['KeyQ', 'KeyW', 'KeyE'],
  PlayerTwoCriticalHitCombination: ['KeyU', 'KeyI', 'KeyO']
}